package com.zsq.compress;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class JSCompress {

    public static final String jarPath = "lib/closure-compiler-v20181125.jar";

    /*private static final Set<String> excludeNames = new HashSet<String>(){{
        add("idangerous.swiper.min.js");
        add("jquery-1.9.1.min.js");
    }};*/

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        List<List<String>> all = new ArrayList<>();
        File file = new File("test\\js\\");
        File[] files = file.listFiles((e) -> e.getName().endsWith(".js") && !e.getName().endsWith(".min.js"));
        for (File f : files) {
            List<String> commands = new ArrayList<String>();
            commands.add("java");
            commands.add("-jar");
            commands.add(jarPath);
            String name = f.getName();
            String oldFilePath = f.getCanonicalPath();
            String newFileName = name.substring(0, name.lastIndexOf(".js")) + ".min.js";
            String newFilePath = f.getParentFile().getParentFile().getCanonicalPath() + File.separator + newFileName;
            commands.add("--js");
            commands.add(oldFilePath);
            commands.add("--js_output_file");
            commands.add(newFilePath);

            all.add(commands);
        }

        //all.forEach(System.out::println);
        for (List<String> list : all) {
            ProcessBuilder builder = new ProcessBuilder(list);
            Process start = builder.start();
            try {
                int i = start.waitFor();
                System.out.println(i + "---0代表成功");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                start.destroy();
            }
        }
        System.out.println("运行结束了！！！");
    }
}
